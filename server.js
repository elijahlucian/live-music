import express from 'express'
import path from 'path'
import yaml from 'js-yaml'
import fs from 'fs'

const covers = yaml.safeLoad(fs.readFileSync('cover-songs.yml'))

const PORT = 3000

class Server {
  constructor(params) {
    this.app = params.app

    // routes
    this.app.get('/', (req,res) => {
      res.sendFile(path.join(__dirname, 'static/index.html'))
    })
    // sockets
  }

  start() {
    this.app.listen(PORT, () => console.log(`Server is running on ${PORT}`))
  }
}

const server = new Server({app: express()})
server.start()

export default server

